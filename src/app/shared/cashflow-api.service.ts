import {Injectable} from '@angular/core';
import {EntryViewModel} from "../entries/entry-add/add-entry.component";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Entry} from "../entries/model/entry";

@Injectable({
  providedIn: 'root'
})
export class CashflowApiService {
  private BASE_URL = "http://localhost:8090/cashflow-management/api";
  private POST_ENTRY = `${this.BASE_URL}\\entries`;
  private GET_ALL_ENTRIES = `${this.BASE_URL}\\entries`;

  constructor(private http: HttpClient) {

  }

  postEntry(entry: EntryViewModel): Observable<Entry> {
    return this.http.post<Entry>(this.POST_ENTRY, entry);
  }

  fetchAllEntries(): Observable<Entry[]> {
    return this.http.get<Entry[]>(this.GET_ALL_ENTRIES);
  }
}
