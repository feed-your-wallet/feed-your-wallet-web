import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Account} from "../entries/entry-add/model/account";
import {Category} from "../entries/entry-add/model/category";

@Injectable({
  providedIn: 'root'
})
export class DictionaryApiService {
  private BASE_URL = "http://localhost:8080/dictionary/api";
  private ALL_ACCOUNTS_URL = `${this.BASE_URL}\\accounts`;
  private ALL_CATEGORIES_URL = `${this.BASE_URL}\\categories`;

  constructor(private http: HttpClient) {

  }

  fetchAllAccounts(): Observable<Account[]> {
    return this.http.get<Account[]>(this.ALL_ACCOUNTS_URL);
  }

  fetchAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.ALL_CATEGORIES_URL)
  }
}
