import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavigationComponent} from './navigation/navigation.component';
import {AccountsComponent} from './accounts/accounts.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {AddEntryComponent} from './entries/entry-add/add-entry.component';
import {StatsComponent} from './stats/stats.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from "@angular/common/http";
import { EntriesComponent } from './entries/entries.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    AccountsComponent,
    NotFoundComponent,
    AddEntryComponent,
    StatsComponent,
    EntriesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
