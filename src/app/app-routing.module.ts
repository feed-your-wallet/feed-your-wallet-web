import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccountsComponent} from "./accounts/accounts.component";
import {AddEntryComponent} from "./entries/entry-add/add-entry.component";
import {StatsComponent} from "./stats/stats.component";
import {NotFoundComponent} from "./not-found/not-found.component";
import {EntriesComponent} from "./entries/entries.component";

const routes: Routes = [
  {
    path: 'accounts',
    component: AccountsComponent
  },
  {
    path: 'entries',
    component: EntriesComponent
  },
  {
    path: 'stats',
    component: StatsComponent
  },
  {
    path: "entry/add",
    component: AddEntryComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
