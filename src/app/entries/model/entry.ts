export interface Entry {

  id: number,
  type: string,
  accountId: number,
  accountLabel: string,
  accountColor: string,
  amount: number,
  currency: string,
  categoryId: number,
  categoryLabel: string,
  categoryColor: string,
  date: string,
  description: string,
  paymentType: string;
}
