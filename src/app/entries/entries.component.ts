import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {CashflowApiService} from "../shared/cashflow-api.service";
import {Entry} from "./model/entry";
import {DictionaryApiService} from "../shared/dictionary-api.service";
import {Account} from "./entry-add/model/account";
import {Category} from "./entry-add/model/category";

@Component({
  selector: 'app-entries',
  templateUrl: './entries.component.html',
  styleUrls: ['./entries.component.css']
})
export class EntriesComponent implements OnInit {
  entries: Entry[] = [];
  accounts: Account[] = [];
  categories: Category[] = [];

  constructor(private http: HttpClient, private dictionaryApiService: DictionaryApiService,
              private cashflowApiService: CashflowApiService) { }

  ngOnInit(): void {
    this.initEntries();
  }

  private async initEntries() {
    this.entries = await this.cashflowApiService.fetchAllEntries().toPromise();
    this.accounts = await this.dictionaryApiService.fetchAllAccounts().toPromise();
    this.categories = await this.dictionaryApiService.fetchAllCategories().toPromise();
    this.enrichEntries();
  }

  private enrichEntries() {
    for (let entry of this.entries) {

      for (let account of this.accounts) {
        if (entry.accountId === account.id) {
          entry.accountLabel = account.name;
          break;
        }
      }

      for (let category of this.categories) {
        if (entry.categoryId === category.id) {
          entry.categoryLabel = category.name;
          break;
        }
      }
    }
  }
}
