import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Category} from "./model/category";
import {Account} from "./model/account";
import {PaymentType} from "./model/payment-type";
import {EntryType} from "./model/entry-type";
import {DictionaryApiService} from "../../shared/dictionary-api.service";
import {CashflowApiService} from "../../shared/cashflow-api.service";
import {formatDate} from "@angular/common";

@Component({
  selector: 'add-entry',
  templateUrl: './add-entry.component.html',
  styleUrls: ['./add-entry.component.css']
})
export class AddEntryComponent implements OnInit {
  model: EntryViewModel = {
    type: 'EXPENSE',
    accountId: 0,
    amount: 0,
    currency: 'PLN',
    categoryId: 0,
    date: formatDate(Date.now(), "yyyy-MM-dd'T'HH:mm", 'en'),
    description: '',
    paymentType: 'CASH'
  };
  entryTypes: EntryType[] = [
    {id: 'EXPENSE', name: 'Expense'},
    {id: 'INVOME', name: 'Income'},
    {id: 'TRANSFER', name: 'Transfer'}
  ];
  accounts: Account[] = [];
  categories: Category[] = [];
  paymentTypes: PaymentType[] = [
    {id: 'CASH', name: 'Cash'},
    {id: 'CARD', name: 'Card'},
    {id: 'BANK_TRANSFER', name: 'Bank transfer'}];

  constructor(
    private http: HttpClient,
    private dictionaryApiService: DictionaryApiService,
    private cashflowApiService: CashflowApiService) {
  }

  ngOnInit(): void {
    this.initData()
  }

  private initData() {
    this.initAccounts();
    this.initCategories();
  }

  private initAccounts() {
    this.dictionaryApiService.fetchAllAccounts().subscribe(
      response => {
        this.accounts = response;
        this.setFirstAccountAsSelected();
      },
      error => {
        alert("An error has occurred");
      }
    )
  }

  private initCategories() {
    this.dictionaryApiService.fetchAllCategories().subscribe(
      response => {
        this.categories = response;
        this.setFirstCategoryAsSelected();
      },
      error => {
        alert("An error has occurred");
      }
    )
  }

  private setFirstAccountAsSelected() {
    for (let i = 0; i < this.accounts.length; i++) {
      if (i == 0 || this.model.accountId > this.accounts[i].id) {
        this.model.accountId = this.accounts[i].id;
      }
    }
  }

  private setFirstCategoryAsSelected() {
    for (let i = 0; i < this.categories.length; i++) {
      if (i == 0 || this.model.categoryId > this.categories[i].id) {
        this.model.categoryId = this.categories[i].id;
      }
    }
  }

  saveEntry(): void {
    this.cashflowApiService.postEntry(this.model).subscribe(
      res => {
        location.reload();
      },
      err => {
        alert("An error has occurred while saving entry")
      }
    );
  }
}

export interface EntryViewModel {
  type: string,
  accountId: number,
  amount: number,
  currency: string,
  categoryId: number,
  date: string,
  description: string,
  paymentType: string;
}
