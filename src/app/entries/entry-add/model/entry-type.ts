export interface EntryType {

  id: string,
  name: string;
}
